$(document).ready(function () {

  $('#team_fred').click(function(e){
    e.preventDefault();
    $('#box_alain').addClass('hide');
    $('#box_seb').addClass('hide');
    $('#box_fred').removeClass('hide');

    $('#team_alain').removeClass('bold-string');
    $('#team_seb').removeClass('bold-string');
    $(this).addClass('bold-string');
  });

  $('#team_alain').click(function(e){
    e.preventDefault();
    $('#box_fred').addClass('hide');
    $('#box_seb').addClass('hide');
    $('#box_alain').removeClass('hide');

    $('#team_fred').removeClass('bold-string');
    $('#team_seb').removeClass('bold-string');
    $(this).addClass('bold-string');
  });

  $('#team_seb').click(function(e){
    e.preventDefault();
    $('#box_alain').addClass('hide');
    $('#box_fred').addClass('hide');
    $('#box_seb').removeClass('hide');

    $('#team_alain').removeClass('bold-string');
    $('#team_fred').removeClass('bold-string');
    $(this).addClass('bold-string');
  });

});
